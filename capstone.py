from abc import ABC, abstractmethod

class Person(ABC):
    def __init__(self, first_name, last_name):
        self.__first_name = first_name
        self.__last_name = last_name
        self.requests = []
        self.users = []

    @property
    def first_name(self):
        return self.__first_name

    @property
    def last_name(self):
        return self.__last_name

    @abstractmethod
    def specificMethod(self):
        pass

    def getFullName(self):
        return f"{self.first_name} {self.last_name}"

    def addRequest(self, request):
        self.requests.append(request)
        return f"Request {request.name} has been added"

    def checkRequest(self, request):
        pass

    def addUser(self):
        pass

    def login(self):
        return f"{self.getFullName()} has logged in"

    def logout(self):
        return f"{self.getFullName()} has logged out"

class Employee(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name)
        self.__email = email
        self.__department = department

    @property
    def email(self):
        return self.__email

    @property
    def department(self):
        return self.__department

    @department.setter
    def department(self, new_department):
        self.__department = new_department

    def specificMethod(self):
        pass

    def login(self):
        return f"{self.email} has logged in"

    def logout(self):
        return f"{self.email} has logged out"

class TeamLead(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name)
        self.__email = email
        self.__department = department
        self.members = []

    @property
    def email(self):
        return self.__email

    @property
    def department(self):
        return self.__department

    @department.setter
    def department(self, new_department):
        self.__department = new_department

    def specificMethod(self):
        pass

    def addMember(self, employee):
        self.members.append(employee)
        return f"{employee.getFullName()} has been added to your team"

class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name)
        self.__email = email
        self.__department = department

    @property
    def email(self):
        return self.__email

    @property
    def department(self):
        return self.__department

    @department.setter
    def department(self, new_department):
        self.__department = new_department

    def specificMethod(self):
        pass

    def addUser(self):
        return "New user added"

class Request:
    def __init__(self, name, requester, date_requested):
        self.name = name
        self.requester = requester
        self.date_requested = date_requested
        self.status = "Open"

    def updateRequest(self, status):
        self.status = status
        return f"Request {self.name} has been {status.lower()}"

    def closeRequest(self):
        return self.updateRequest("Closed")

    def cancelRequest(self):
        return self.updateRequest("Cancelled")

# Test case
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest(req2) == "Request Laptop repair has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.members:
    print(indiv_emp.getFullName())

assert admin1.addUser() == "New user added"

req2.updateRequest("Closed")
print(req2.closeRequest())
